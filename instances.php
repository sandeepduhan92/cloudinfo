<?php
 
// set up DB
$conn = mysql_connect("localhost", "gwuser", "gwp@sswd");
mysql_select_db("ssh_gw");

// set your db encoding -- for ascent chars (if required)
mysql_query("SET NAMES 'utf8'");

// include and create object
include("inc/jqgrid_dist.php");

// custom columns to display
$col = array();
$col["title"] = "INST ID";
$col["name"] = "ID";
$col["align"] = "center";
$col["width"] = "150";
$col["link"] = "http://cloudinfo.u2opia.local/instance_ACL.php?ID={ID}";
$cols[] = $col;

$col = array();
$col["title"] = "NAME";
$col["name"] = "NAME";
$col["width"] = "250";
$cols[] = $col;

$col = array();
$col["title"] = "PUBLIC IP";
$col["name"] = "PUBLIC_IP";
$col["width"] = "120";
$cols[] = $col;

$col = array();
$col["title"] = "PRIVATE IP";
$col["name"] = "PRIVATE_IP";
$col["width"] = "120";
$cols[] = $col;

$col = array();
$col["title"] = "TYPE";
$col["name"] = "INSTANCE_TYPE";
$col["width"] = "90";
$cols[] = $col;

$col = array();
$col["title"] = "AVAILABILITY ZONE";
$col["name"] = "ZONE";
$col["width"] = "120";
$cols[] = $col;

$col = array();
$col["title"] = "STATUS";
$col["name"] = "STATE";
$col["width"] = "80";
$cols[] = $col;

$col = array();
$col["title"] = "AWS REGION";
$col["name"] = "AWS_REGION";
$col["width"] = "100";
$cols[] = $col;

$g = new jqgrid();

// set Grid params
$grid["caption"] = "List of instances on the AWS Cloud";
$grid["height"] = "415";
$grid["width"] = "1200";
$grid["multiselect"] = true;
$grid["rowList"] = array(50,'All');
$grid["rowNum"] = 50;
#$grid["export"] = array("format"=>"pdf", "filename"=>"cloudinfo-export", "sheetname"=>"test");
#$grid["export"] = array("filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape", "paper"=>"a4");

$g->set_options($grid);
$g->set_actions(array(  
                        "add" => false,
                        "edit" => false,
                        "delete" => false,
                        "export" => true,
                        "search" => "simple",
                    ) 
                );

$g->table = "instances";
//$g->select_command = "SELECT ID,NAME,PUBLIC_IP,PRIVATE_IP,INSTANCE_TYPE,ZONE,AWS_REGION,DESCRIPTION FROM instances";
$g->set_columns($cols);

// subqueries are also supported now (v1.2)
// $g->select_command = "select * from (select * from invheader) as o";
			
// render grid
$out = $g->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" media="screen" href="js/themes/redmond/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
</head>
<body>
        <b><i>* Partial search using wildcard - %</i></b><br>
        <b><i>* VPC private IP range: 10.117.x.x, 10.141.x.x, 10.121.x.x, 10.93.x.x</i></b>
	<div style="margin:10px">
	<?php echo $out?>
	</div>
</body>
</html>

<?php
 
// set up DB
$conn = mysql_connect("localhost", "gwuser", "gwp@sswd");
mysql_select_db("ssh_gw");

// set your db encoding -- for ascent chars (if required)
mysql_query("SET NAMES 'utf8'");

// include and create object
include("inc/jqgrid_dist.php");

// custom columns to display
$col = array();
$col["title"] = "INST ID";
$col["name"] = "INSTANCE_ID";
$col["align"] = "center";
$col["width"] = "20";
$cols[] = $col;

$col = array();
$col["title"] = "NAME";
$col["name"] = "NAME";
$col["width"] = "40";
$cols[] = $col;

$col = array();
$col["title"] = "PUBLIC IP";
$col["name"] = "PUBLIC_IP";
$col["width"] = "30";
$cols[] = $col;

$col = array();
$col["title"] = "INST TYPE";
$col["name"] = "INSTANCE_TYPE";
$col["width"] = "20";
$cols[] = $col;

$col = array();
$col["title"] = "AVAILABILITY ZONE";
$col["name"] = "ZONE";
$col["width"] = "30";
$cols[] = $col;

$col = array();
$col["title"] = "AWS REGION";
$col["name"] = "AWS_REGION";
$col["width"] = "20";
$cols[] = $col;

$col = array();
$col["title"] = "SUDO ACCESS";
$col["name"] = "SUDO_ACCESS";
$col["width"] = "15";
#$col["resizable"] = false;
$cols[] = $col;

$g = new jqgrid();

// set Grid params
$user_id = $_GET['ID'];
$grid["caption"] = "Access control list for user ID: $user_id";
$grid["height"] = "530";
$grid["width"] = "1200";
$grid["multiselect"] = true;
$grid["rowList"] = array(50,'All');
$grid["rowNum"] = 50;
//$grid["autowidth"] = true;
//$grid["sortname"] = "ID"; 
//$grid["sortorder"] = "desc";

$g->set_options($grid);
$g->set_actions(array(  
                        "add" => false,
                        "edit" => false,
                        "delete" => false,
                        "export" => true,
                        "search" => "simple",
                    ) 
                );

$g->select_command = "SELECT INSTANCE_ID,NAME,PUBLIC_IP,INSTANCE_TYPE,ZONE,AWS_REGION,SUDO_ACCESS FROM ACL WHERE USER_ID='$user_id'";
//$g->table = "ACL";
$g->set_columns($cols);

// render grid
$out = $g->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" media="screen" href="js/themes/redmond/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
</head>
<body>
	<div style="margin:10px">
	<?php echo $out?>
	</div>
</body>
</html>

<?php
 
// set up DB
$conn = mysql_connect("localhost", "gwuser", "gwp@sswd");
mysql_select_db("ssh_gw");

// set your db encoding -- for ascent chars (if required)
mysql_query("SET NAMES 'utf8'");

// include and create object
include("inc/jqgrid_dist.php");

// custom columns to display
$col = array();
$col["title"] = "UNIQUE ID";
$col["name"] = "ID";
$col["align"] = "center";
$col["width"] = "30";
$col["link"] = "http://cloudinfo.u2opia.local/user_ACL.php?ID={ID}";
$cols[] = $col;

$col = array();
$col["title"] = "LOGIN ID";
$col["name"] = "USERNAME";
$col["width"] = "60";
$cols[] = $col;

$col = array();
$col["title"] = "USER GROUP";
$col["name"] = "USERGROUP";
$col["width"] = "40";
$cols[] = $col;

$g = new jqgrid();

// set Grid params
$grid["caption"] = "List of users on the AWS Cloud";
$grid["height"] = "530";
$grid["width"] = "400";
$grid["multiselect"] = true;
$grid["rowList"] = array(50,'All');
$grid["rowNum"] = 50;

$g->set_options($grid);
$g->set_actions(array(  
                        "add" => false,
                        "edit" => false,
                        "delete" => false,
                        "export" => true,
                        "search" => "simple",
                    ) 
                );

$g->table = "users";
$g->set_columns($cols);

// render grid
$out = $g->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" media="screen" href="js/themes/redmond/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
</head>
<body>
	<div style="margin:10px">
	<?php echo $out?>
	</div>
</body>
</html>
